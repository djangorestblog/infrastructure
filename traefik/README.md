## Traefik

### Full installation process

1. Ssh to server
2. Move into directory opt
 
 `cd /opt/`
 
3. Download traefik - [here](https://github.com/containous/traefik/releases/download/v2.0.5/traefik_v2.0.5_linux_amd64.tar.gz)
4. Unzip 

 `tar -zxvf traefik_file_name.tar.gz`

5. Optionally remove tar file with License and readme.
6. Install Docker. [Instructions for Ubuntu 18](https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru)
7. Install docker-compose. [Instructions for Ubuntu 18](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)
8. Add user traefik
 
 `sudo useradd traefik`

9. Add user to group 

`sudo usermod -a -G docker traefik`

10.Create traefik directory and prepare

`sudo mkdir /etc/traefik`

`sudo mkdir /etc/traefik/acme`

`sudo touch /etc/traefik/config.yml`

`sudo touch /etc/traefik/traefik.yml`

11. Paste data from the config.yml into `/etc/traefik/config.yml` 

12. Paste data from the traefik.yml into `/etc/traefik/traefik.yml`

_These are default settings. Feel free to edit._

* Default Login - **admin**

* Default Password - **superadmin**

13. Create system service file

`sudo touch /etc/systemd/system/traefik.service`

13. Paste data from the traefik.service into `/etc/systemd/system/traefik.service`

14. Start service

`sudo systemctl start traefik`

15. Check service

`sudo systemctl status traefik`
